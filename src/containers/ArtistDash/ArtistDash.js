import React, {Component} from 'react';
import {Container, Grid} from 'semantic-ui-react'
import DaoRating from '../../components/ArtistDashComponent/DaoRating/DaoRating';
import PlayPlace from '../../components/ArtistDashComponent/PlayPlace/PlayPlace';
import TypeCity from '../../components/ArtistDashComponent/TypeCity/TypeCity';
import SocialStats from '../../components/ArtistDashComponent/SocialStats/SocialStats';
import ShowAdvertising from '../../components/ArtistDashComponent/ShowAdvertising/ShowAdvertising';

class ArtistDash extends Component {
    render() {
        return (
            <Container>
                <Grid>
                    <Grid.Column width={11}>
                        <DaoRating/>
                        <PlayPlace/>
                        <TypeCity/>
                    </Grid.Column>
                    <Grid.Column width={5}>
                        <ShowAdvertising/>
                        <SocialStats/>
                    </Grid.Column>
                </Grid>
            </Container>
        );
    }
}

export default ArtistDash;