import React, {Component} from 'react';
import {Container, Grid} from 'semantic-ui-react';

import VenueBanner from '../../components/VenueOverviewComponents/VenueBanner/VenueBanner';
import VenueStatistics from '../../components/VenueOverviewComponents/VenueStatistics/VenueStatistics';
import SocialStatistics from '../../components/VenueOverviewComponents/SocialStatistics/SocialStatistics';
import VenueOverviewActive from '../../components/VenueOverviewComponents/VenueOverviewActive/VenueOverviewActive';
import SuggestedArtist from '../../components/VenueOverviewComponents/SuggestedArtist/SuggestedArtist';
import VenueOverviewPending from '../../components/VenueOverviewComponents/VenueOverviewPending/VenueOverviewPending';
import CalendarVenue from '../../components/CalendarElements/CalendarVenue/CalendarVenue';
import VenueDropdown from '../../components/CalendarElements/VenueDropdown/VenueDropdown';

class VenueOverview extends Component {

    render() {
        return (
            <Container>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={11}>
                            <CalendarVenue/>
                            <VenueDropdown/>
                        </Grid.Column>
                        <Grid.Column width={5}>
                            <VenueBanner/>
                            <VenueStatistics/>
                            <SocialStatistics/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={9}>
                        </Grid.Column>
                        <Grid.Column width={7}>
                            <VenueOverviewActive/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={9}>
                        </Grid.Column>
                        <Grid.Column width={7}>
                            <SuggestedArtist/>
                            <VenueOverviewPending/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        )
    }
}

export default VenueOverview;
