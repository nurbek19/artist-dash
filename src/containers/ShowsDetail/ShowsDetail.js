import React, {Component} from 'react';
import {Container, Grid} from 'semantic-ui-react';

import ShowBanner from '../../components/ShowsDetailComponents/ShowBanner/ShowBanner';
import Influencers from '../../components/ShowsDetailComponents/Influencers/Influencers';
import InfluencersActivity from '../../components/ShowsDetailComponents/InfluencerActivity/InfluencerActivity';
import Chat from '../../components/ShowsDetailComponents/Chat/Chat';
import SocialChannels from '../../components/ShowsDetailComponents/SocialChannels/SocialChannels';
import SocialEvent from '../../components/ShowsDetailComponents/SocialEvent/SocialEvent';

class ShowsDetail extends Component {

    render() {
        return (
            <Container>
                <Grid>
                    <Grid.Column width={11}>
                        <ShowBanner/>
                        <Influencers/>
                        <InfluencersActivity/>
                    </Grid.Column>
                    <Grid.Column width={5}>
                        <Chat/>
                        <SocialChannels/>
                        <SocialEvent/>
                    </Grid.Column>
                </Grid>
            </Container>
        )
    }
}

export default ShowsDetail;