import React, {Component, Fragment} from 'react';
import {Container, Grid} from 'semantic-ui-react';
import VenueForms from '../../components/VenueShowsComponenets/VenueForms/VenueForms';
import VenueSlider from '../../components/VenueShowsComponenets/VenueSlider/VenueSlider';
import Artists from '../../components/VenueShowsComponenets/Artists/Artists';
import SelectedArtist from '../../components/VenueShowsComponenets/SelectedArtist/SelectedArtist';
import ArtistsList from '../../components/VenueShowsComponenets/ArtistsList/ArtistsList';
import './VenueShows.css';

class VenueShows extends Component {

    render() {
        return (
            <Fragment>
                <div className="venue-show-form">
                    <Container>
                        <Grid>
                            <Grid.Row>
                                <VenueForms/>
                            </Grid.Row>
                        </Grid>
                    </Container>
                </div>

                <Container>
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column width={12}>
                                <VenueSlider/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column width={12}>
                                <Artists/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>

                <div className="selected-artist-container">
                    <Container>
                        <Grid centered>
                            <Grid.Column width={12}>
                                <SelectedArtist/>
                            </Grid.Column>
                        </Grid>
                    </Container>
                </div>

                <Container>
                    <ArtistsList/>
                </Container>
            </Fragment>
        )
    }
}

export default VenueShows;