import React, {Component} from 'react';
import {Container, Grid} from 'semantic-ui-react';

import ShowsTabs from '../../components/ShowsListComponents/ShowsTabs/ShowsTabs';
import './ShowList.css';

class ShowsList extends Component {

    render() {
        return(
            <Container className="show-list">
                <Grid>
                    <Grid.Column width={4}>
                        <div className="remaining-tickets">
                            <h2>3,400</h2>
                            <p>Tickets remain to be sold for upcoming shows</p>
                        </div>
                    </Grid.Column>

                    <Grid.Column width={6}>
                        <div className="sold-tickets">
                            <h2>494</h2>
                            <p>Tickets sold this week</p>
                        </div>
                    </Grid.Column>

                    <Grid.Column width={6}>
                        <div className="show-paragraph">
                            <p>Your upcoming show at Mezzanine is among 25 most popular events in California at the moment. Wohoo!</p>
                        </div>
                    </Grid.Column>

                    <Grid.Column width={16}>
                        <ShowsTabs/>
                    </Grid.Column>
                </Grid>
            </Container>
        )
    }
}

export default ShowsList;