import React, {Component} from 'react';
import {Container, Grid} from 'semantic-ui-react';
import VenueShowsTabs from '../../components/VenueShowsTableComponents/VenueShowsTabs/VenueShowsTabs';

class VenueShowsTable extends Component {

    render() {
        return (
            <Container>
                <VenueShowsTabs/>
            </Container>
        )
    }
}

export default VenueShowsTable;