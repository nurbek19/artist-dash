import React, {Component} from 'react';
import {Container, Grid, Form, Input} from 'semantic-ui-react';
import TopCities from '../../components/FinderComponent/TopCities/TopCities';
import Recommendations from '../../components/FinderComponent/Recommendations/Recommandations';
import './Finder.css';

class Finder extends Component {
    render() {
        return (
            <Container className="finder-container">
                <Grid>
                    <Grid.Column width={11}>
                        <div className="city-name">
                            <Form.Field control={Input}
                                        label='Check your popularity and explore venues in any given city'
                                        placeholder='Type city name'
                            />
                        </div>
                    </Grid.Column>

                    <Grid.Column width={16}>
                        <TopCities/>
                    </Grid.Column>

                    <Grid.Column width={11}>
                        <Recommendations/>
                    </Grid.Column>
                </Grid>
            </Container>
        )
    }
}

export default Finder;