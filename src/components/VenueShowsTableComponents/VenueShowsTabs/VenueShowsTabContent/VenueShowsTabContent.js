import React, {Component, Fragment} from 'react';
import {Grid} from 'semantic-ui-react';
import VenueShowItem from './VenueShowItem/VenueShowItem';
import VenueBookItem from './VenueBookItem/VenueBookItem';
import './VenueShowsTabContent.css';

class VenueShowsTabContent extends Component {

    showHandler = event => {
        const currentElement = event.target;
        const hiddenRow = currentElement.closest('.event-row').nextSibling;

        if(hiddenRow.className === 'row hidden-row') {
            hiddenRow.className += ' show-row';
            currentElement.style.transform = 'rotate(90deg)';
        } else {
            hiddenRow.className = 'row hidden-row';
            currentElement.style.transform = 'none';
        }

    };

    render() {
        return (
            <Fragment>
                <div className="titles">
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={1}>
                                <p>Details</p>
                            </Grid.Column>
                            <Grid.Column width={1}>
                                <p>Date</p>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <p>Artist</p>
                            </Grid.Column>
                            <Grid.Column width={2}>
                                <p>Tickets Sold</p>
                            </Grid.Column>
                            <Grid.Column width={2}>
                                <p>Revenue</p>
                            </Grid.Column>
                            <Grid.Column width={2}>
                                <p>Dao Rating</p>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <p>Top Influencer</p>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
                <VenueShowItem showHandler={this.showHandler}/>
                <VenueBookItem/>
                <VenueShowItem showHandler={this.showHandler}/>
            </Fragment>
        )
    }
}

export default VenueShowsTabContent;