import React from 'react';
import {Grid, Icon, Button} from 'semantic-ui-react';
import './VenueShowItem.css';
import artist from '../../../../../assets/img/artist.png';
import influencer from '../../../../../assets/img/event-influencer.png';

const VenueShowItem = props => {
    return (
        <div className="about-event">
            <Grid>
                <Grid.Row className="event-row">
                    <Grid.Column width={1}>
                        <Icon name="play circle outline" className="show-icon" onClick={props.showHandler}/>
                    </Grid.Column>
                    <Grid.Column width={1}>
                        <div className="event-date">
                            <h5>Nov</h5>
                            <h4>6</h4>
                        </div>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <div className="participant">
                            <div className="participant-img">
                                <img src={artist} alt="artist"/>
                            </div>

                            <p>Len Faki</p>
                        </div>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <p>400 of 5,000</p>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <p>$20,200</p>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <p>75 <span>(+13%)</span></p>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <div className="participant">
                            <div className="participant-img">
                                <img src={influencer} alt="influencer"/>
                            </div>

                            <p>Yovana Ventura</p>
                        </div>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row className="hidden-row">
                    <Grid.Column width={3} className="influencer-column">
                        <div className="event-influencer">
                            <div className="influencer-img">
                                <img src={influencer} alt="influencer"/>
                            </div>

                            <ul>
                                <li>
                                    <p>Floyd Mayweather</p></li>
                                <li>
                                    <p><span>19.2M</span> reach</p></li>
                                <li>
                                    <p><span>4,494</span> leads</p></li>
                                <li>
                                    <p><span>383</span> tickets sold</p></li>
                            </ul>
                        </div>
                    </Grid.Column>
                    <Grid.Column width={3} className="influencer-column">
                        <div className="event-influencer">
                            <div className="influencer-img">
                                <img src={influencer} alt="influencer"/>
                            </div>

                            <ul>
                                <li>
                                    <p>Floyd Mayweather</p></li>
                                <li>
                                    <p><span>19.2M</span> reach</p></li>
                                <li>
                                    <p><span>4,494</span> leads</p></li>
                                <li>
                                    <p><span>383</span> tickets sold</p></li>
                            </ul>
                        </div>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <div className="extra-information">
                            <p>Box Office</p>
                            <h5>$494,304</h5>
                        </div>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <div className="extra-information">
                            <p>Show Time</p>
                            <h5>8pm</h5>
                        </div>
                    </Grid.Column>
                    <Grid.Column width={3}>
                        <ul className="event-socials">
                            <li>
                                <a href="#facebook">
                                    <Icon name="facebook f"/>
                                </a>
                            </li>
                            <li>
                                <a href="#facebook">
                                    <Icon name="twitter"/>
                                </a>
                            </li>
                            <li>
                                <a href="#facebook">
                                    <Icon name="instagram"/>
                                </a>
                            </li>
                            <li>
                                <a href="#facebook">
                                    <Icon name="tumblr"/>
                                </a>
                            </li>
                            <li>
                                <a href="#facebook">
                                    <Icon name="youtube"/>
                                </a>
                            </li>
                        </ul>
                    </Grid.Column>
                    <Grid.Column width={3}>
                        <Button primary>Event Page</Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </div>
    )
};

export default VenueShowItem;
