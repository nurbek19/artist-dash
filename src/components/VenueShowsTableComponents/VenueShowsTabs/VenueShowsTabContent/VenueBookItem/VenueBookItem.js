import React from 'react';
import {Grid, Button} from 'semantic-ui-react';
import './VenueBookItem.css';

const VenueBookItem = props => {
    return (
        <div className="book-event">
            <Grid>
                <Grid.Row>
                    <Grid.Column width={2}>
                        <div className="free-date">
                            <h5>Nov</h5>
                            <h4>17-24</h4>
                        </div>
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <h3>You have nobody booked for these dates.</h3>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Button primary>Find an artist now</Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </div>
    )
};

export default VenueBookItem;