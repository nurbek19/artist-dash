import React, {Component} from 'react';
import {Tab, Form} from 'semantic-ui-react';
import VenueShowsTabContent from './VenueShowsTabContent/VenueShowsTabContent';
import './VenueShowsTabs.css';

class VenueShowsTabs extends Component {

    render() {
        const panes = [
            {menuItem: 'Upcoming Shows', render: () => <Tab.Pane attached={false}><VenueShowsTabContent/></Tab.Pane>},
            {menuItem: 'Past Shows', render: () => <Tab.Pane attached={false}><VenueShowsTabContent/></Tab.Pane>},
        ];

        return (
            <div className="event-tabs-container">
                <Form.Input placeholder='Search' className="search-field"/>
                <Tab menu={{text: true}} className="event-tabs" panes={panes}/>
            </div>
        )
    }
}

export default VenueShowsTabs;