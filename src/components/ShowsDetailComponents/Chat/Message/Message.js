import React from 'react';
import './Message.css';

const Message = props => {
    return(
        <div className="message">
            <span className="message-author">{props.author}</span>
            <p className="message-body">{props.body}</p>
        </div>
    )
};

export default Message;
