import React from 'react';
import Message from './Message/Message';
import './Chat.css';

const Chat = props => {
    return (
        <div className="chat-container">
            <h6>Chat with John (Mezzanine)</h6>

            <div className="messages">
                <Message
                    author="John Smith (Mezzanine), 2:59PM"
                    body="Hey Bob, great to hear from you. I will look through your proposal and get back to you by tomorrow morning."
                />
                <Message
                    author="You, 3:04PM"
                    body="John, sounds great. Looking forward to it."
                />
            </div>
            
            <div className="send-message">
                <textarea name="message" cols="30" rows="10" placeholder="Type message here">
                </textarea>
                <a href="#">Open Offer Details</a>
            </div>
        </div>
    )
};

export default Chat;