import React from 'react';
import './Influencer.css';

const Influencer = props => {
  return(
      <div className="influencer">
          <div className="influencer-avatar">
              <img src={props.img} alt="influencer"/>
          </div>

          <h6>{props.influencer}</h6>
          <p>{props.reachAmount} <span>reach</span></p>
          <p>{props.likes} <span>likes</span></p>
          <p>{props.tickets} <span>tickets sold</span></p>
      </div>
  )
};

export default Influencer;