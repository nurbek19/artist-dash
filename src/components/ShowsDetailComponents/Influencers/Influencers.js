import React from 'react';
import {Grid} from 'semantic-ui-react';
import Influencer from './Influencer/Influencer';
import './Influencers.css';
import influencerImg from '../../../assets/img/FloydMayweather.png';

const Influencers = props => {
    return (
        <div className="influencers-container">
            <h5>Top 4 Influencers</h5>

            <div className="influencers">
                <Grid>
                    <Grid.Column width={4}>
                        <Influencer
                            img={influencerImg}
                            influencer="Floyd Mayweather"
                            reachAmount="19.2M"
                            likes="383,039"
                            tickets="383"
                        />
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Influencer
                            img={influencerImg}
                            influencer="Floyd Mayweather"
                            reachAmount="19.2M"
                            likes="383,039"
                            tickets="383"
                        />
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Influencer
                            img={influencerImg}
                            influencer="Floyd Mayweather"
                            reachAmount="19.2M"
                            likes="383,039"
                            tickets="383"
                        />
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Influencer
                            img={influencerImg}
                            influencer="Floyd Mayweather"
                            reachAmount="19.2M"
                            likes="383,039"
                            tickets="383"
                        />
                    </Grid.Column>
                </Grid>
            </div>
        </div>
    )
};

export default Influencers;