import React from 'react';
import './SocialChannel.css';

const SocialChannel = props => {
    return(
        <ul className="social-channel">
            <li className="social-title">{props.title}</li>
            <li>{props.influencers} <span>influencers</span></li>
            <li>{props.sold} <span>sold</span></li>
            <li>{props.reposts} <span>reposts</span></li>
            <li>{props.likes} <span>likes</span></li>
        </ul>
    )
};

export default SocialChannel;