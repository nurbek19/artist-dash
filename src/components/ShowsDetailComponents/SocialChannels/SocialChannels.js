import React from 'react';
import './SocialChannels.css';
import SocialChannel from './SocialChannel/SocialChannel';

const SocialChannels = props => {
    return(
        <div className="social-channels">
            <h6>Social Channels Stats</h6>

            <div className="channels-list">
                <SocialChannel
                    title="Facebook"
                    influencers="112"
                    sold="234"
                    reposts="122"
                    likes="2123"
                />
                <SocialChannel
                    title="Twitter"
                    influencers="112"
                    sold="234"
                    reposts="122"
                    likes="2123"
                />
            </div>
        </div>
    )
};

export default SocialChannels;