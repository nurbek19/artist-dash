import React from 'react';
import {Icon} from 'semantic-ui-react';
import './SocialEvent.css';

const SocialEvent = props => {
    return(
        <div className="social-event">
            <h6>Event social media pages</h6>

            <div className="event-title">
                <span className="social-icon">
                    <Icon name="facebook f" size='large'/>
                </span>
                <p>EventBrite</p>
            </div>

            <div className="address">
                <h6>Venue address</h6>
                <p>238 Geary St, <br/> San Francisco, CA</p>
            </div>
        </div>
    )
};

export default SocialEvent;