import React from 'react';
import {Grid, Icon} from 'semantic-ui-react';
import Activity from './Activity/Activity';
import './InfluencerActivity.css';

const InfluencerActivity = props => {
    return (
        <div className="influencers-activity">
            <div className="title-block">
                <h5>Influncer activity</h5>
                <h6>Sort by date <Icon name="caret down" /></h6>
            </div>

            <div className="activities-container">
                <Grid className="activity-grid">
                    <Grid.Column width={8} className="activity-title">
                        <p>Activity</p>
                    </Grid.Column>
                    <Grid.Column width={8} className="rating-title">
                        <p>Rating Change</p>
                    </Grid.Column>

                    <Activity/>
                    <Activity/>
                    <Activity/>
                </Grid>
            </div>
        </div>
    )
};

export default InfluencerActivity;