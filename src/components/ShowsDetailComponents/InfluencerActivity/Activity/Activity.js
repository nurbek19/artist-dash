import React, {Fragment} from 'react';
import {Grid, Feed, Icon} from 'semantic-ui-react';
import './Activity.css';
import avatar from '../../../../assets/img/FloydMayweather.png';

const Activity = props => {
    return (
        <Fragment>
            <Grid.Column width={13}>
                <div className="activity">
                    <Icon size='big' name="facebook f" className="activity-icon facebook"/>
                    <Feed>
                        <Feed.Event>
                            <Feed.Label image={avatar}/>
                            <Feed.Content>
                                <Feed.Summary>
                                    Nick Bateman on Facebook
                                    <Feed.Date>15 September, 1:30pm</Feed.Date>
                                </Feed.Summary>
                                <Feed.Extra text>
                                    When i workout, Bob Moses music keeps me going!
                                </Feed.Extra>
                                <Feed.Meta>
                                    <a href="#">
                                        <Icon name='comment'/>
                                        642
                                    </a>
                                    <a href="#">
                                        <Icon name='retweet'/>
                                        4.1k
                                    </a>
                                    <a href="#">
                                        <Icon name='like'/>
                                        13k
                                    </a>
                                </Feed.Meta>
                            </Feed.Content>
                        </Feed.Event>
                    </Feed>
                </div>
            </Grid.Column>
            <Grid.Column width={3} className="activity-rating">
                <h2>0.7%</h2>
            </Grid.Column>
        </Fragment>
    )
};

export default Activity;