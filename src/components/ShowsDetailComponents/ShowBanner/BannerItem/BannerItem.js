import React from 'react';
import './BannerItem.css';

const BannerItem = props => {
    return(
        <div className="banner-item">
            <h3>{props.title}</h3>
            <p>{props.text}</p>
        </div>
    )
};

export default BannerItem;