import React, {Fragment} from 'react';
import BannerItem from './BannerItem/BannerItem';
import './ShowBanner.css';
import ticketGraphic from '../../../assets/img/ticket-price-graphic.png';

const ShowBanner = () => {
    return (
        <Fragment>
            <div className="show-banner">
                <div className="overlay-banner"></div>
                <h2>October 15</h2>
                <h1>Mezzanine</h1>
                <h4>San Francisco, CA</h4>

                <div className="tickets-information">
                    <BannerItem title="$25" text="Average ticket price"/>
                    <BannerItem title="5,845" text="Tickets sold"/>
                    <BannerItem title="484" text="Tickets remaining"/>
                    <BannerItem title="$139,494" text="Revenue projection"/>
                </div>
            </div>

            <div className="ticket-price-container">
                <h5>Ticket price: $39</h5>

                <div className="ticket-price-graphic">
                    <ul>
                        <li>$45</li>
                        <li>$20</li>
                        <li></li>
                    </ul>

                    <img src={ticketGraphic} alt="graphic"/>
                </div>

                <ul className="graphic-month">
                    <li>August</li>
                    <li>September</li>
                    <li>October</li>
                </ul>
            </div>

            <div className="analysis-graphic">
                <h5>Media comments analysys</h5>
                <p>We track and analyze comments about your event from all possible sources.</p>

                <ul className="comment-type">
                    <li>
                        <span></span>
                        Negative
                    </li>
                    <li>
                        Positive
                        <span></span>
                    </li>
                </ul>

                <div className="comment-gradient">
                    <div className="comment-indicator">
                        <div className="gradient-slider">
                            <span></span>
                        </div>
                        <p>The comments are mostly positive. Yay!</p>
                    </div>
                </div>
            </div>

            <div className="rating-container">
                <h5>Your weDao rating since this event campaign has started</h5>
                <p>weDao rating is compromised of a number of parameters, learn more here</p>

                <div className="rating-graphic-block">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>

                    <img src={ticketGraphic} alt="graphic"/>
                </div>

                <ul className="rating-month">
                    <li>August</li>
                    <li>September</li>
                    <li>October</li>
                </ul>
            </div>
        </Fragment>
    )
};

export default ShowBanner;