import React from 'react';
import {Grid, Form, Button} from 'semantic-ui-react';
import './Artist.css';
import artist from '../../../../assets/img/FloydMayweather.png';

const Artist = props => {
    return (
        <Grid>
            <Grid.Row className="artist">
                <Grid.Column width={2}>
                    <Form.Checkbox />
                </Grid.Column>
                <Grid.Column width={3}>
                    <div className="artist-img">
                        <img src={artist} alt="artist img"/>
                    </div>
                </Grid.Column>
                <Grid.Column width={6}>
                    <p className="about-artist">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aperiam est provident repellat sit.
                    </p>
                </Grid.Column>
                <Grid.Column width={3}>
                    <Button secondary className="invite-btn">INVITE</Button>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
};

export default Artist;