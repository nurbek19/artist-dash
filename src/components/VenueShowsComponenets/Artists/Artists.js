import React, {Component} from 'react';
import Artist from './Artist/Artist';
import './Artists.css';

class Artists extends Component {

    render() {
        return(
            <div className="artists">
                <h6>Select <br/> Multiple</h6>
                <Artist/>
                <Artist/>
                <Artist/>

            </div>
        )
    }
}

export default Artists;