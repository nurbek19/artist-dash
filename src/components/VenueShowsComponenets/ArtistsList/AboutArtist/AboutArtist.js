import React, {Component} from 'react';
import {Grid, Button, Icon} from 'semantic-ui-react';
import './AboutArtist.css';
import artistImg from '../../../../assets/img/tiesto.png';
import ratingImg from '../../../../assets/img/rating.png';

class AboutArtist extends Component {

    render() {
        return (
            <Grid.Row>
                <Grid.Column width={6}>
                    <div className="artist-avatar">
                        <img src={artistImg} alt="artist"/>
                    </div>
                </Grid.Column>
                <Grid.Column width={10}>
                    <div className="about-artist">
                        <div className="artists-contacts">
                            <div>
                                <h3>Tiesto</h3>
                                <a href="#tiesto">View Profile</a>
                            </div>
                            <div className="social-links">
                                <ul>
                                    <li>
                                        <a href="#facebook">
                                            <Icon name="facebook f" size="large"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#facebook">
                                            <Icon name="twitter" size="large"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#facebook">
                                            <Icon name="instagram" size="large"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#facebook">
                                            <Icon name="tumblr" size="large"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#facebook">
                                            <Icon name="youtube" size="large"/>
                                        </a>
                                    </li>
                                </ul>

                                <div className="action-btns">
                                    <Button>Show Interest</Button>
                                    <Button>Invite</Button>
                                </div>
                            </div>
                        </div>

                        <div className="artist-rating">
                            <div>
                                <h6>Dao Rating</h6>
                                <h4>75 (+13%)</h4>
                            </div>
                            <div className="rating-graphic">
                                <img src={ratingImg} alt="rating"/>
                            </div>
                        </div>

                        <ul className="inf-list">
                            <li>
                                <h6>Avg Ticket Price</h6>
                                <h4>$50</h4>
                            </li>
                            <li>
                                <h6>Potential revenue</h6>
                                <h4>$39,494</h4>
                            </li>
                            <li>
                                <h6>Audience</h6>
                                <h4>3M+</h4>
                            </li>
                        </ul>

                        <div className="respond-block">
                            <h6>Usually responds within:</h6>
                            <p>7 days</p>
                        </div>
                    </div>
                </Grid.Column>
            </Grid.Row>
        )
    }
}

export default AboutArtist;