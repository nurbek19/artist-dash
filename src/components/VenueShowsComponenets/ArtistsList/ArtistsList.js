import React, {Component} from 'react';
import {Grid} from 'semantic-ui-react';
import AboutArtist from './AboutArtist/AboutArtist';
import './ArtistsList.css';

class ArtistList extends Component {

    render() {
        return (
            <Grid className="artists-list">
                <AboutArtist/>
                <AboutArtist/>
            </Grid>
        )
    }
}

export default ArtistList;
