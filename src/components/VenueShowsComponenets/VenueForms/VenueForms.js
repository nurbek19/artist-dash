import React, {Fragment, Component} from 'react';
import {Grid, Form} from 'semantic-ui-react';
import './VenueForms.css';

const options = [
    {key: 't', text: 'Techno', value: 'techno'},
    {key: 'r', text: 'Rock', value: 'rock'},
    {key: 'p', text: 'Pop', value: 'pop'}
];

class VenueForms extends Component {
    state = {};

    handleChange = (e, {value}) => this.setState({value});

    render() {
        const {value} = this.state;

        return (
            <Fragment>
                <Grid.Column width={3}>
                    <div className="show-date">
                        <p>When</p>
                        <h5>November 1 - 15</h5>
                        <a href="#change">Change</a>
                    </div>
                </Grid.Column>
                <Grid.Column width={13}>
                    <form action="#" className="venue-forms">
                        <Form.Group className="form-group">
                            <Form.Select label='Style' options={options} placeholder='Style'/>
                            <Form.Input label='Search for artist or keyword' placeholder='Type'/>
                        </Form.Group>

                        <Form.Group className="radio-btn-group">
                            <Form.Radio label='Single Performance' value='sp' checked={value === 'sp'}
                                        onChange={this.handleChange}/>
                            <Form.Radio label='Lineup' value='lp' checked={value === 'lp'}
                                        onChange={this.handleChange}/>
                        </Form.Group>
                    </form>
                </Grid.Column>
            </Fragment>
        )
    }
}
;

export default VenueForms;