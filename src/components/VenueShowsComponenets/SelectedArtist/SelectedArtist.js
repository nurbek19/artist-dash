import React from 'react';
import {Button} from 'semantic-ui-react';
import './SelectedArtist.css';
import artistImg from '../../../assets/img/FloydMayweather.png';

const SelectedArtist = props => {
    return(
        <div className="selected-artist">
            <h5>Selected Artist</h5>

            <div className="artists-container">
                <div className="artists-img">
                    <div className="artist-img-wrapper">
                        <img src={artistImg} alt="artist img"/>
                    </div>
                    <div className="artist-img-wrapper">
                        <img src={artistImg} alt="artist img"/>
                    </div>
                    <div className="artist-img-wrapper">
                        <img src={artistImg} alt="artist img"/>
                    </div>
                    <div className="artist-img-wrapper">
                        <img src={artistImg} alt="artist img"/>
                    </div>
                </div>
                <Button secondary className="invite-btn">Send Invite To All 4</Button>
            </div>
        </div>
    )
};

export default SelectedArtist;