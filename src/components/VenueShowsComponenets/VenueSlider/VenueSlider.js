import React, {Component} from 'react'
import Slider from 'react-slick';
import './VenueSlider.css';
import sliderImg from '../../../assets/img/banner.jpg';

function SampleNextArrow(props) {
    const {className, style, onClick} = props;
    return (
        <div
            className={className}
            style={{...style}}
            onClick={onClick}
        ></div>
    );
}

function SamplePrevArrow(props) {
    const {className, style, onClick} = props;
    return (
        <div
            className={className}
            style={{...style}}
            onClick={onClick}
        ></div>
    );
}

class VenueSlider extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <div className="venue-slider">
                <Slider {...settings}>
                    <div>
                        <img src={sliderImg} alt="slider-img"/>
                    </div>
                    <div>
                        <img src={sliderImg} alt="slider-img"/>
                    </div>
                    <div>
                        <img src={sliderImg} alt="slider-img"/>
                    </div>
                    <div>
                        <img src={sliderImg} alt="slider-img"/>
                    </div>
                    <div>
                        <img src={sliderImg} alt="slider-img"/>
                    </div>
                    <div>
                        <img src={sliderImg} alt="slider-img"/>
                    </div>
                </Slider>
            </div>
        );
    }
}

export default VenueSlider;
