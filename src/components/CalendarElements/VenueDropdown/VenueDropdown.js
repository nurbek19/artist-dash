import React from 'react';
import {Button} from 'semantic-ui-react';

import './VenueDropdown.css';
import venueMember from '../../../assets/img/venue-member.png';

const VenueDropdown = props => {
    return (
        <div className="venue-dropdown">
            <ul className="about-member">
                <li className="first-list">
                    <div className="member-title">
                        <h3>Bass Nectar</h3>
                        <p>March 15th, 8pm</p>
                    </div>

                    <div className="member-avatar">
                        <img src={venueMember} alt="venue member"/>
                    </div>
                </li>

                <li>
                    <h5>500 of 550 <span>(95%)</span></h5>
                    <p>tickets sold</p>
                </li>

                <li>
                    <h5>$30,000</h5>
                    <p>revenue</p>
                </li>
            </ul>
            <Button>Go to event page</Button>
        </div>
    )
};

export default VenueDropdown;