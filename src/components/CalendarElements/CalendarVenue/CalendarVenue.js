import React from 'react';
import {Progress} from 'semantic-ui-react';

import './CalendarVenue.css';

const CalendarVenue = props => {
    return (
        <div className="calendar-venue">
            <div className="calendar-venue-overlay"></div>
            <div className="calendar-venue-content">
                <span className="venue-number">5</span>
                <h4>Eastern Sun</h4>

                <Progress percent={44} progress color='green'/>
            </div>
        </div>
    )
};

export default CalendarVenue;