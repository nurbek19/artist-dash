import React, {Component} from 'react';
import {Tab, Table, Input} from 'semantic-ui-react';
import ShowTabContent from './ShowTabContent/ShowTabContent';
import DatePickerForm from '../../../containers/DatePickerForm/DatePickerForm';
import './ShowsTabs.css';

const panes = [
    {
        menuItem: 'San Francisco, CA',
        render: () => <Tab.Pane attached={false}><ShowTabContent /></Tab.Pane>
    },
    {
        menuItem: 'Los Angeles, CA',
        render: () => <Tab.Pane attached={false}><ShowTabContent /></Tab.Pane>
    }
];

class ShowsTabs extends Component {

    render() {
        return (
            <div className="shows-tabs">
                <Tab menu={{text: true}} panes={panes}/>

                <div className="show-filter">
                    <Input placeholder='Search...'/>

                    <div className="date-range">
                        <p>Set date range</p>
                        <DatePickerForm />
                        <DatePickerForm />
                    </div>
                </div>
            </div>
        )
    }
}

export default ShowsTabs;