import React, {Component} from 'react';
import {Table} from 'semantic-ui-react';
import ShowTabItem from './ShowTabItem/ShowTabItem';

import './ShowTabContent.css';
import showImg from '../../../../assets/img/show-img.png';
import influencer from '../../../../assets/img/influencer.png';

class ShowTabContent extends Component {

    render() {
        return (
            <div className="show-tab-content">

                <Table basic='very' className="show-table">
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={4} className="table-head">Date</Table.HeaderCell>
                            <Table.HeaderCell width={3} className="table-head">City</Table.HeaderCell>
                            <Table.HeaderCell width={2} className="table-head">Venue</Table.HeaderCell>
                            <Table.HeaderCell width={2}  className="table-head">Tickets Sold</Table.HeaderCell>
                            <Table.HeaderCell width={2}  className="table-head">Dao Rating</Table.HeaderCell>
                            <Table.HeaderCell width={3}  className="table-head">Top Influencers</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        <ShowTabItem
                            month="Nov"
                            day="5"
                            showImg={showImg}
                            city="San Francisco, CA"
                            venue="Mezzannine"
                            soldTickets="400 of 5,000"
                            daoRating="75 (+13%)"
                            influencerImg={influencer}
                            influencer="Yovana Ventura"
                        />

                        <ShowTabItem
                            month="Nov"
                            day="5"
                            showImg={showImg}
                            city="San Francisco, CA"
                            venue="Mezzannine"
                            soldTickets="400 of 5,000"
                            daoRating="75 (+13%)"
                            influencerImg={influencer}
                            influencer="Yovana Ventura"
                        />

                        <ShowTabItem
                            month="Nov"
                            day="5"
                            showImg={showImg}
                            city="San Francisco, CA"
                            venue="Mezzannine"
                            soldTickets="400 of 5,000"
                            daoRating="75 (+13%)"
                            influencerImg={influencer}
                            influencer="Yovana Ventura"
                        />

                        <ShowTabItem
                            month="Nov"
                            day="5"
                            showImg={showImg}
                            city="San Francisco, CA"
                            venue="Mezzannine"
                            soldTickets="400 of 5,000"
                            daoRating="75 (+13%)"
                            influencerImg={influencer}
                            influencer="Yovana Ventura"
                        />
                    </Table.Body>
                </Table>

            </div>
        )
    }
}

export default ShowTabContent;