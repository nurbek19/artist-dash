import React from 'react';
import {Table} from 'semantic-ui-react'
import './ShowTabItem.css';

const ShowTabItem = props => {
    return (
        <Table.Row verticalAlign='middle' className="show-detail">
            <Table.Cell className="table-cell">
                <div className="first-cell">
                    <div className="date">
                        <h5>{props.month}</h5>
                        <h4>{props.day}</h4>
                    </div>

                    <div className="show-img">
                        <img src={props.showImg} alt="show-img"/>
                    </div>
                </div>
            </Table.Cell>
            <Table.Cell className="table-cell">{props.city}</Table.Cell>
            <Table.Cell className="table-cell">{props.venue}</Table.Cell>
            <Table.Cell className="table-cell">{props.soldTickets}</Table.Cell>
            <Table.Cell className="table-cell">{props.daoRating}</Table.Cell>
            <Table.Cell className="table-cell">
                <div className="last-cell">
                    <div className="influencer-avatar">
                        <img src={props.influencerImg} alt="influencer image"/>
                    </div>

                    <p>{props.influencer}</p>
                </div>
            </Table.Cell>
        </Table.Row>
    )
};

export default ShowTabItem;
