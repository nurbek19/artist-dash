import React from 'react';
import {Table} from 'semantic-ui-react'
import './TopCityItem.css';

const TopCityItem = props => {
    return (
        <Table.Row verticalAlign='top'>
            <Table.Cell className="table-cell">{props.rank}</Table.Cell>
            <Table.Cell className="table-cell">{props.city}</Table.Cell>
            <Table.Cell className="table-cell">{props.potentialRevenue}</Table.Cell>
            <Table.Cell className="table-cell">{props.existingAudience}</Table.Cell>
            <Table.Cell className="table-cell">{props.potentialAudience}</Table.Cell>
            <Table.Cell className="table-cell best-venue">{props.bestVenue}</Table.Cell>
            <Table.Cell className="table-cell">{props.influencers}</Table.Cell>
        </Table.Row>
    )
};

export default TopCityItem;