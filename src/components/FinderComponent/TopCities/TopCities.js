import React, {Component} from 'react';
import {Table} from 'semantic-ui-react';
import TopCitiesItem from './TopCityItem/TopCityItem';
import './TopCities.css';

class TopCities extends Component {

    render() {
        return (
            <div className="top-cities">
                <h2>Top cities</h2>
                <p>We analyze a ton of data to show you where you are most popular</p>

                <Table basic='very' className="top-cities-table" >
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={1} className="table-head">Rank</Table.HeaderCell>
                            <Table.HeaderCell width={2} className="table-head">City</Table.HeaderCell>
                            <Table.HeaderCell width={2} className="table-head">Potential Revenue</Table.HeaderCell>
                            <Table.HeaderCell width={2}  className="table-head">Existing Audience</Table.HeaderCell>
                            <Table.HeaderCell width={2}  className="table-head">Potential Audience</Table.HeaderCell>
                            <Table.HeaderCell width={3}  className="table-head">Best Match Venue</Table.HeaderCell>
                            <Table.HeaderCell width={4}  className="table-head">Top Influencers</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <TopCitiesItem
                            rank="56"
                            city="San Francisco"
                            potentialRevenue="$9,000"
                            existingAudience="45,000"
                            potentialAudience="65,000"
                            bestVenue={null}
                            influencers="Yovana Ventura, John Smith and 5 more"
                        />

                        <TopCitiesItem
                            rank="56"
                            city="San Francisco"
                            potentialRevenue="$9,000"
                            existingAudience="45,000"
                            potentialAudience="65,000"
                            bestVenue={null}
                            influencers="Yovana Ventura, John Smith and 5 more"
                        />

                        <TopCitiesItem
                            rank="56"
                            city="San Francisco"
                            potentialRevenue="$9,000"
                            existingAudience="45,000"
                            potentialAudience="65,000"
                            bestVenue={null}
                            influencers="Yovana Ventura, John Smith and 5 more"
                        />

                        <TopCitiesItem
                            rank="56"
                            city="San Francisco"
                            potentialRevenue="$9,000"
                            existingAudience="45,000"
                            potentialAudience="65,000"
                            bestVenue={null}
                            influencers="Yovana Ventura, John Smith and 5 more"
                        />

                        <TopCitiesItem
                            rank="56"
                            city="San Francisco"
                            potentialRevenue="$9,000"
                            existingAudience="45,000"
                            potentialAudience="65,000"
                            bestVenue={null}
                            influencers="Yovana Ventura, John Smith and 5 more"
                        />
                    </Table.Body>
                </Table>

                <a href="#" className="view-more">View more</a>
            </div>
        )
    }
}

export default TopCities;