import React from 'react';
import {Tab} from 'semantic-ui-react';
import TabContent from './TabContent/TabContent';
import './Recommandations.css';

const panes = [
    {
        menuItem: 'San Francisco, CA',
        render: () => <Tab.Pane attached={false} className="tabs-stage"><TabContent /></Tab.Pane>
    },
    {
        menuItem: 'Los Angeles, CA',
        render: () => <Tab.Pane attached={false} className="tabs-stage"><TabContent /></Tab.Pane>
    },
    {
        menuItem: 'New York, CA',
        render: () => <Tab.Pane attached={false} className="tabs-stage"><TabContent /></Tab.Pane>
    },
];

const Recommandations = () => {
    return (
        <div className="recommendations">
            <div className="recommendation-title">
                <h3 className="r-title">Recommendations for your next show</h3>
                <p className="r-text">Take a look at the proposed cities and venues. If you like it, show your
                    interest.</p>
            </div>

            <div className="tabs-container">
                <Tab menu={{text: true, className: "tabs-nav"}} panes={panes}/>
                <a href="#" className="view-all">View all</a>
            </div>

        </div>
    )
};

export default Recommandations;