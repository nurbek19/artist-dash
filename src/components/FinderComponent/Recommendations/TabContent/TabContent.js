import React from 'react';
import {Grid, Button} from 'semantic-ui-react';
import './TabContent.css';

const TabContent = props => {

    return (
        <div className="tab-content">
            <Grid>
                <Grid.Column width={11}>
                    <div className="show-place">
                        <div>
                            <p className="s-text">Potential Venue</p>
                            <h5 className="s-title">Mezzanine</h5>
                        </div>

                        <Button className="s-button">Show Interest</Button>
                    </div>
                </Grid.Column>

                <Grid.Column width={5}>
                    <ul className="about-show">
                        <li className="list-text">
                            <p className="s-text">Existing Audience 13,000</p>
                        </li>
                        <li className="list-text">
                            <p className="s-text">Potential Audience 18,000</p>
                        </li>
                        <li className="list-text">
                            <p className="s-text">Potential Influencers Steve Johnson</p>
                        </li>
                        <li className="list-text">
                            <p className="s-text">Potential Ticket Sales $39,000</p>
                        </li>
                    </ul>
                </Grid.Column>
            </Grid>
        </div>
    )
};

export default TabContent;