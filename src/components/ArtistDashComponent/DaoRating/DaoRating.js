import React from 'react';
import './DaoRating.css';
import line from '../../../assets/img/Vector.png';

const DaoRating = props => {
    return(
        <div className="dao-rating">
            <div className="title">
                <h4>Dao Rating</h4>
                <a href="#">How to boost rating</a>
            </div>

            <div className="graphic">
                <img src={line} alt="graphic"/>
            </div>
        </div>
    )
};

export default DaoRating;