import React from 'react';
import { Form } from 'semantic-ui-react';
import './Place.css';

const options = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
];

const Place = props => {
    return(
        <div className="place">
            <div className="place-amount">
                <h2>{props.amount}</h2>
                <p> recommended <br/> {props.text} in</p>
            </div>

            <Form.Select options={options} placeholder={props.placeholder}/>
        </div>
    )
};

export default Place;