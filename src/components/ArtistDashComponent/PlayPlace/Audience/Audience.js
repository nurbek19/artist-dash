import React from 'react';
import './Audience.css';

const Audience = props => {
  return(
      <div className="audience">
          <h5>{props.amount}</h5>
          <p>{props.text}</p>
      </div>
  )
};

export default Audience;