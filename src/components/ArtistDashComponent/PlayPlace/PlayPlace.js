import React from 'react';
import {Button} from 'semantic-ui-react';
import Place from './Place/Place';
import Audience from './Audience/Audience';
import './PlayPlace.css'

const PlayPlace = props => {
    return (
        <div className="play-place">
            <h2>Find where to play next</h2>
            <p>Scroll through the venues and select the ones where you’d like to play. And what happens next?</p>

            <div className="banner">
                <div className="banner-overlay"></div>
                <div className="recommended-places">
                    <Place amount="25" text="cities" placeholder="United States"/>
                    <Place amount="15" text="venues" placeholder="San Francisco"/>
                </div>

                <div className="place-title">
                    <div>
                        <h1>Mezzanine</h1>
                        <h3>San Francisco, CA </h3>
                    </div>

                    <Button>I’d play here</Button>
                </div>

                <div className="audiences">
                    <Audience amount="122,100" text="Existing Audience"/>
                    <Audience amount="124,100" text="Potential Audience"/>
                    <Audience amount="$39,304" text="Potential revenue"/>
                </div>
            </div>
        </div>
    )
};

export default PlayPlace;