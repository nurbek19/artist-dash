import React from 'react';
import {Progress} from 'react-sweet-progress';
import "react-sweet-progress/lib/style.css";
import './ShowAdvertising.css';

const ShowAdvertising = props => {
    return (
        <div className="advertising">
            <div className="show-title">
                <h3>Next Show</h3>
                <a href="#">View All</a>
            </div>

            <div className="show-date">
                <span>In 23 days</span>
                <h2>MAR 4</h2>
                <h4>@ Warfield <br/> San Francisco, CA</h4>
            </div>

            <div className="tickets-identifier">
                <span>Tickets sold</span>

                <Progress
                    type="circle"
                    width={240}
                    percent={70}
                    strokeWidth={20}
                    stroke="#000"
                />

                <span className="available-tickets">832 available</span>
                <span>Revenue</span>
            </div>

        </div>
    )
};

export default ShowAdvertising;
