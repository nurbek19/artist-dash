import React from 'react';
import './TypeCity.css';

const TypeCity = props => {
    return(
        <div className="type-city">
            <h3>Check your popularity and explore venues in any given city</h3>
            <textarea name="city-name" id="city-name" cols="30" rows="10" placeholder="Type city name"/>
        </div>
    )
};

export default TypeCity;