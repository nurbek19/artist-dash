import React from 'react';
import SocialItem from './SocialItem/SocialItem';
import './SocialStats.css';

const SocialStats = props => {
    return (
        <div className="social-stats">
            <h3>Social Stats</h3>
            <SocialItem social="Facebook" amount="12,949" percent="5%"/>
            <SocialItem social="Twitter" amount="12,949" percent="5%"/>
            <SocialItem social="Instagram" amount="12,949" percent="5%"/>
            <SocialItem social="Youtube" amount="12,949" percent="5%"/>

            <a href="#">View Analytics</a>
        </div>
    )
};

export default SocialStats;