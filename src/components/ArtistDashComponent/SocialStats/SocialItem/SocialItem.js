import React from 'react';
import './SocialItem.css';

const SocialItem = props => {
  return(
      <div className="social-item">
          <p>{props.social} followers</p>
          <div className="followers">
              <h2>{props.amount}</h2>
              <span>{props.percent}</span>
          </div>
      </div>
  )
};

export default SocialItem;