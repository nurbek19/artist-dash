import React from 'react';
import './VenueArtist.css';

import chatIcon from '../../../../assets/img/chat-icon.png';

const VenueArtist = props => {
    return(
        <div className="venue-artist">
            <div className="artist-short-descr">
                <img src={props.img} alt="venue artist"/>
                <h5>{props.name}</h5>
            </div>

            <img src={chatIcon} alt="chat icon"/>
        </div>
    )
};

export default VenueArtist;