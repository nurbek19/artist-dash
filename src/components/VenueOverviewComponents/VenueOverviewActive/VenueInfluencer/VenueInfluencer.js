import React from 'react';
import './VenueInfluencer.css';

const VenueInfluencer = props => {
    return(
        <div className="venue-influencer">
            <img src={props.img} alt="influencer"/>
            <h5>{props.name}</h5>
        </div>
    )
};

export default VenueInfluencer;
