import React from 'react';
import {Grid, Icon} from 'semantic-ui-react';
import {Progress} from 'react-sweet-progress';
import "react-sweet-progress/lib/style.css";

import VenueArtist from './VenueArtist/VenueArtist';
import VenueInfluencer from './VenueInfluencer/VenueInfluencer';
import './VenueOverviewActive.css';
import graphic from '../../../assets/img/overview-active-vector.png';
import overviewArtist from '../../../assets/img/overview-artist.png';
import overviewInfluencer from '../../../assets/img/nick-bateman.png';

const VenueOverviewActive = props => {
    return (
        <div className="overview-active">
            <div className="overview-active-overlay"></div>
            <div className="overview-close-btn">
                <Icon circular inverted name="close" />
            </div>
            <div className="overview-active-content">
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={9}>
                            <div className="overview-active-description">
                                <h3>Eastern Sun</h3>

                                <div className="overview-date">
                                    <p>In 23 days</p>
                                    <h1>MAR 4</h1>
                                </div>

                                <h4>8pm</h4>
                                <h4>@ Warfield San Francisco, CA</h4>
                            </div>
                        </Grid.Column>
                        <Grid.Column width={7} className="second-column">
                            <div className="tickets-percentage-circle">
                                <Progress
                                    type="circle"
                                    width={140}
                                    percent={75}
                                    strokeWidth={10}
                                />
                                <span>Tickets sold</span>

                                <p><span>832</span> available</p>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={9}>
                            <div className="overview-active-rating">
                                <h5>Dao Rating 75 (+13%)</h5>
                                <img src={graphic} alt="venue overview graphic"/>
                            </div>
                        </Grid.Column>
                        <Grid.Column width={7} className="second-column">
                            <div className="revenue">
                                <h5>Revenue</h5>
                                <h3>$49,304</h3>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <div className="overview-artists">
                                <h5>Artists</h5>

                                <VenueArtist name="Len Faki" img={overviewArtist}/>
                                <VenueArtist name="Len Faki" img={overviewArtist}/>
                                <VenueArtist name="Len Faki" img={overviewArtist}/>
                            </div>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={16}>
                            <div className="overview-influencers">
                                <h5>Influencers</h5>

                                <VenueInfluencer name="Nick Bateman" img={overviewInfluencer}/>
                                <VenueInfluencer name="Nick Bateman" img={overviewInfluencer}/>
                                <VenueInfluencer name="Nick Bateman" img={overviewInfluencer}/>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </div>
    )
};

export default VenueOverviewActive;