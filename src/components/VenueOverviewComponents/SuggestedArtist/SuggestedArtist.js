import React from 'react';
import {Grid, Icon, Button} from 'semantic-ui-react';

import './SuggestedArtist.css';
import suggestedArtistImg from '../../../assets/img/amelie-lens.png';

const SuggestedArtist = props => {
    return (
        <div className="suggested-artist">
            <div className="suggested-artist-overlay"></div>
            <Icon circular inverted name="close" className="close-btn"/>

            <div className="suggested-artist-content">
                <Grid>
                    <Grid.Row verticalAlign='middle'>
                        <Grid.Column width={6}>
                            <div className="suggested-artist-avatar">
                                <img src={suggestedArtistImg} alt="sugeested artist"/>
                            </div>
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <div className="about-suggested-artist">
                                <p>Suggested artist</p>
                                <h1>Amelie Lens</h1>
                            </div>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={6}>
                            <div className="invite-btn">
                                <Button>Send Invite</Button>
                            </div>
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <div className="short-information">
                                <h5>Potential Revenue</h5>
                                <h4>$52,781</h4>
                            </div>

                            <div className="short-information">
                                <h5>Latest shows</h5>
                                <p>MAR 1</p>
                                <p>Greek Theater, Los Angeles, CA</p>
                            </div>

                            <div className="short-information">
                                <p>FEB 10</p>
                                <p>Cow Hollow, San Francisco, CA</p>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </div>
    )
};

export default SuggestedArtist;