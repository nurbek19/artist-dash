import React from 'react';
import {Grid} from 'semantic-ui-react';

import VenueStatisticsItem from './VenueStatisticsItem/VenueStatisticsItem';
import './VenueStatistics.css';

const VenueStatistics = props => {
    return (
        <div className="venue-statistics">
            <Grid>
                <VenueStatisticsItem
                    title="15"
                    text="Shows this month"
                />
                <VenueStatisticsItem
                    title="2,400"
                    text="Tickets sold this month"
                />
                <VenueStatisticsItem
                    title="$49,304"
                    text="Revenue last 30 days"
                />
                <VenueStatisticsItem
                    title="$85,505"
                    text="Total box office last 30 days"
                />
            </Grid>
        </div>
    )
};

export default VenueStatistics;
