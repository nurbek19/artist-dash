import React from 'react';
import {Grid} from 'semantic-ui-react';
import './VenueStatisticsItem.css';

const VenueStatisticsItem = props => {
    return (
        <Grid.Column width={8}>
            <div className="statistic-item">
                <h3>{props.title}</h3>
                <p>{props.text}</p>
            </div>
        </Grid.Column>
    )
};

export default VenueStatisticsItem;