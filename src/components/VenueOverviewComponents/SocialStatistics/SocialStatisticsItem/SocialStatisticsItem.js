import React from 'react';
import './SocialStatisticsItem.css';

const SocialStatisticsItem = props => {
    return(
        <div className="social-statistic-item">
            <p><span>{props.social}</span> followers</p>
            <div className="statistic-information">
                <h3>{props.amount}</h3>
                <p>{props.percent}</p>
            </div>
        </div>
    )
};

export default SocialStatisticsItem;