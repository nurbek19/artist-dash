import React from 'react';

import SocialStatisticsItem from './SocialStatisticsItem/SocialStatisticsItem';
import './SocialStatistics.css';

const SocialStatistics = props => {
    return(
        <div className="social-statistics">
            <div className="social-statistics-title">
                <h4>Social Stats</h4>
                <a href="#view-analytics">View Analytics</a>
            </div>

            <SocialStatisticsItem
                social="Facebook"
                amount="12,949"
                percent="5%"
            />

            <SocialStatisticsItem
                social="Twitter"
                amount="3,500"
                percent="1%"
            />

            <SocialStatisticsItem
                social="Instagram"
                amount="383,494"
                percent="15%"
            />
        </div>
    )
};

export default SocialStatistics;