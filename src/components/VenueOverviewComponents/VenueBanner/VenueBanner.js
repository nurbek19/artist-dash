import React from 'react';
import {Progress} from 'react-sweet-progress';
import "react-sweet-progress/lib/style.css";

import './VenueBanner.css';

const VenueBanner = props => {
    return (
        <div className="venue-banner">
            <div className="venue-banner-overlay"></div>
            <div className="banner-content">
                <p>March 5</p>
                <h1>Eastern Sun</h1>

                <div className="percentage-circle">
                    <Progress
                        type="circle"
                        width={245}
                        percent={75}
                        strokeWidth={10}
                    />
                    <span>Tickets sold</span>
                </div>

                <div className="available-tickets">
                    <p><span>832</span> available</p>
                </div>
            </div>
        </div>
    )
};

export default VenueBanner;