import React from 'react';
import {Grid, Icon} from 'semantic-ui-react';

import './VenueOverviewPending.css';
import AmeliLens from '../../../assets/img/artist-avatar.png';
import overviewArtist from '../../../assets/img/overview-artist.png';
import chatIcon from '../../../assets/img/chat-icon.png';
import dUnity from '../../../assets/img/d-unity.png';

const VenueOverviewPending = props => {
    return (
        <div className="overview-active">
            <div className="overview-active-overlay"></div>
            <div className="overview-close-btn">
                <Icon circular inverted name="close"/>
            </div>
            <div className="overview-active-content">
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={9}>
                            <div className="overview-active-description">
                                <h3>Eastern Sun</h3>

                                <div className="overview-date">
                                    <p>In 23 days</p>
                                    <h1>MAR 4</h1>
                                </div>

                                <h4>8pm</h4>
                                <h4>@ Warfield San Francisco, CA</h4>
                            </div>
                        </Grid.Column>
                        <Grid.Column width={7} className="second-column">
                            <div className="pending-block">
                                <div className="pending-title">
                                    <span className="pending-icon"></span>
                                    <h5>Pending</h5>
                                </div>

                                <p>25% completed</p>
                            </div>

                            <div className="potential-revenue">
                                <h5>Potential Revenue</h5>
                                <h3>$52,781</h3>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <div className="overview-artists">
                                <h5>Artists</h5>

                                <div className="artist-block">
                                    <div className="artist-picture">
                                        <img src={AmeliLens} alt="artist"/>
                                    </div>

                                    <div className="artist-information">
                                        <div className="artist-name">
                                            <h5>Amelie Lens</h5>
                                            <p>0%</p>
                                        </div>

                                        <p>Amelie requested $30,000</p>

                                        <div className="buttons">
                                            <button>Confirm</button>
                                            <span>/</span>
                                            <button>Change</button>
                                        </div>
                                    </div>

                                    <div className="chat-icon">
                                        <div>
                                            <img src={chatIcon} alt="chat icon"/>
                                        </div>
                                    </div>
                                </div>

                                <div className="artist-block">
                                    <div className="artist-picture">
                                        <img src={overviewArtist} alt="artist"/>
                                    </div>

                                    <div className="artist-information">
                                        <div className="artist-name">
                                            <h5>Len Faki</h5>
                                            <p>25%</p>
                                        </div>

                                        <p><span></span> Len confirmed the date</p>
                                    </div>

                                    <div className="chat-icon">
                                        <div>
                                            <img src={chatIcon} alt="chat icon"/>
                                        </div>
                                    </div>
                                </div>

                                <div className="artist-block">
                                    <div className="artist-picture">
                                        <img src={dUnity} alt="artist"/>
                                    </div>

                                    <div className="artist-information">
                                        <div className="artist-name">
                                            <h5>D-Unity</h5>
                                            <p>25%</p>
                                        </div>
                                    </div>

                                    <div className="chat-icon">
                                        <div>
                                            <img src={chatIcon} alt="chat icon"/>
                                            <span className="new-message">1</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </div>
    )
};

export default VenueOverviewPending;