import React, {Component, Fragment} from 'react';
import {Switch, Route, NavLink} from 'react-router-dom';
import ArtistDash from "./containers/ArtistDash/ArtistDash";
import Finder from "./containers/Finder/Finder";
import ShowsList from "./containers/ShowsList/ShowsList";
import ShowsDetail from "./containers/ShowsDetail/ShowsDetail";
import VenueShows from "./containers/VenueShows/VenueShows";
import VenueShowsTable from "./containers/VenueShowsTable/VenueShowsTable";
import VenueOverview from "./containers/VenueOverview/VenueOverview";

class App extends Component {
    render() {
        return (
            <Fragment>
                <ul style={{display: 'flex', justifyContent: 'space-around', listStyle: 'none'}}>
                    <li>
                        <NavLink to="/artist-dash">Artist Dash</NavLink>
                    </li>
                    <li>
                        <NavLink to="/finder">Finder</NavLink>
                    </li>
                    <li>
                        <NavLink to="/shows-list">Shows List</NavLink>
                    </li>
                    <li>
                        <NavLink to="/shows-detail">Shows Detail</NavLink>
                    </li>
                    <li>
                        <NavLink to="/venue-shows">Venue Shows</NavLink>
                    </li>
                    <li>
                        <NavLink to="/venue-shows-table">Venue Shows Table</NavLink>
                    </li>
                    <li>
                        <NavLink to="/venue-overview">Venue Overview</NavLink>
                    </li>
                </ul>

                <Switch>
                    <Route path="/artist-dash" component={ArtistDash} />
                    <Route path="/finder" component={Finder} />
                    <Route path="/shows-list" component={ShowsList} />
                    <Route path="/shows-detail" component={ShowsDetail} />
                    <Route path="/venue-shows" exact component={VenueShows} />
                    <Route path="/venue-shows-table" exact component={VenueShowsTable} />
                    <Route path="/venue-overview" exact component={VenueOverview} />
                </Switch>
            </Fragment>
        );
    }
}

export default App;
